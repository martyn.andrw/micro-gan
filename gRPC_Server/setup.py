import os
import cv2
import math
import pickle
import imageio
import warnings
import PIL.Image
import numpy as np
from PIL import Image
import tensorflow as tf
from random import randrange
import moviepy.editor as mpy
# from google.colab import drive
# from google.colab import files
import matplotlib.pyplot as plt
from IPython.display import clear_output
from moviepy.video.io.ffmpeg_writer import FFMPEG_VideoWriter
warnings.filterwarnings("ignore")

def get_watermarked(pil_image: Image) -> Image:
    try:
        image = cv2.cvtColor(np.array(pil_image), cv2.COLOR_RGB2BGR)
        (h, w) = image.shape[:2]
        image = np.dstack([image, np.ones((h, w), dtype="uint8") * 255])
        pct = 0.08
        full_watermark = cv2.imread('media/logo.png', cv2.IMREAD_UNCHANGED)
        (fwH, fwW) = full_watermark.shape[:2]
        wH = int(pct * h*2)
        wW = int((wH * fwW) / fwH*0.1)
        watermark = cv2.resize(full_watermark, (wH, wW), interpolation=cv2.INTER_AREA)
        overlay = np.zeros((h, w, 4), dtype="uint8")
        (wH, wW) = watermark.shape[:2]
        overlay[h - wH - 10 : h - 10, 10 : 10 + wW] = watermark
        output = image.copy()
        cv2.addWeighted(overlay, 0.5, output, 1.0, 0, output)
        rgb_image = cv2.cvtColor(output, cv2.COLOR_BGR2RGB)
        return Image.fromarray(rgb_image)
    except: return pil_image

def generate_final_images(latent_vector, direction, coeffs, i):
    new_latent_vector = latent_vector.copy()
    new_latent_vector[:8] = (latent_vector + coeffs*direction)[:8]
    new_latent_vector = new_latent_vector.reshape((1, 18, 512))
    generator.set_dlatents(new_latent_vector)
    img_array = generator.generate_images()[0]
    img = PIL.Image.fromarray(img_array, 'RGB')
    if size[0] >= 512: img = get_watermarked(img)
    img_path = "for_animation/" + str(i) + ".png"
    img.thumbnail(animation_size, PIL.Image.ANTIALIAS)
    img.save(img_path)
    face_img.append(imageio.imread(img_path))
    clear_output()
    return img

def generate_final_image(latent_vector, direction, coeffs):
    new_latent_vector = latent_vector.copy()
    new_latent_vector[:8] = (latent_vector + coeffs*direction)[:8]
    new_latent_vector = new_latent_vector.reshape((1, 18, 512))
    generator.set_dlatents(new_latent_vector)
    img_array = generator.generate_images()[0]
    img = PIL.Image.fromarray(img_array, 'RGB')
    if size[0] >= 512: img = get_watermarked(img)
    img.thumbnail(size, PIL.Image.ANTIALIAS)
    img.save("face.png")
    if download_image == True: files.download("face.png")
    return img

def plot_three_images(imgB, fs = 10):
    f, axarr = plt.subplots(1,3, figsize=(fs,fs))
    axarr[0].imshow(Image.open('gRPC_Server/aligned_images/father_01.png'))
    axarr[0].title.set_text('Фото отца')
    axarr[1].imshow(imgB)
    axarr[1].title.set_text('Фото ребенка')
    axarr[2].imshow(Image.open('gRPC_Server/aligned_images/mother_01.png'))
    axarr[2].title.set_text('Фото матери')
    plt.setp(plt.gcf().get_axes(), xticks=[], yticks=[])
    plt.show()

if not os.path.exists("gRPC_Server/aligned_images"):
    os.mkdir("gRPC_Server/aligned_images")

if not os.path.exists("gRPC_Server/data"):
    os.mkdir("gRPC_Server/data")

if not os.path.exists("gRPC_Server/father_image"):
    os.mkdir("gRPC_Server/father_image")

if not os.path.exists("gRPC_Server/mother_image"):
    os.mkdir("gRPC_Server/mother_image")

import config
import dnnlib
import dnnlib.tflib as tflib
from encoder.generator_model import Generator

age_direction = np.load('gRPC_Server/ffhq_dataset/latent_directions/age.npy')
horizontal_direction = np.load('gRPC_Server/ffhq_dataset/latent_directions/angle_horizontal.npy')
vertical_direction = np.load('gRPC_Server/ffhq_dataset/latent_directions/angle_vertical.npy')
eyes_open_direction = np.load('gRPC_Server/ffhq_dataset/latent_directions/eyes_open.npy')
gender_direction = np.load('gRPC_Server/ffhq_dataset/latent_directions/gender.npy')
smile_direction = np.load('gRPC_Server/ffhq_dataset/latent_directions/smile.npy')

# clear_output()
print("setup: OK")