import string
from datetime import datetime

import pkg.UploadImages_pb2 as pkg
import pkg.UploadImages_pb2_grpc as pkg_grpc
import align_images

from concurrent import futures
import grpc
import os

import requests
import shutil

from PIL import Image
import PIL.Image

def downloadImage(url: string, path: string):
    filename = path + url.split("/")[-1].split(".")[-1]

    r = requests.get(url, stream = True)
    if r.status_code == 200:
        r.raw.decode_content = True
        
        with open(filename,'wb') as f:
            shutil.copyfileobj(r.raw, f)
            
        return 'Image sucessfully downloaded: ' + filename
    else:
        return 'Image couldn\'t be downloaded: ' + url
    return "error"

def log(answer: string):
    print(datetime.now().strftime("%H:%M:%S") + " " + answer)

class UploadImageService (pkg_grpc.UploadImageServiceServicer):
    def UploadImage(self, request, context):
        answer = ""

        if (request.meta.parent_type != pkg.Metadata.FATHER) and (request.meta.parent_type != pkg.Metadata.MOTHER):
            context.abort(grpc.StatusCode.NOT_FOUND, "Parent type not found")
        
        if request.meta.parent_type == pkg.Metadata.FATHER:
            for path in os.listdir("gRPC_Server/father_image"):
                os.remove("gRPC_Server/father_image/" + path)

            log(downloadImage(request.url, "gRPC_Server/father_image/father."))
            log(align_images.find_face("gRPC_Server/father_image", "gRPC_Server/aligned_images"))

            if os.path.isfile('gRPC_Server/aligned_images/father_01.png'):
                pil_father = Image.open('gRPC_Server/aligned_images/father_01.png')
                (fat_width, fat_height) = pil_father.size
                resize_fat = max(fat_width, fat_height)/256
                log("Father\'s face was uploaded and found\n")
                # display(pil_father.resize((int(fat_width/resize_fat), int(fat_height/resize_fat))))
            else: log('Father\'s face was not found or there is more than one in the photo.')

        if request.meta.parent_type == pkg.Metadata.MOTHER:
            for path in os.listdir("gRPC_Server/mother_image"):
                os.remove("gRPC_Server/mother_image/" + path)

            log(downloadImage(request.url, "gRPC_Server/mother_image/mother."))
            log(align_images.find_face("gRPC_Server/mother_image", "gRPC_Server/aligned_images"))

            if os.path.isfile('gRPC_Server/aligned_images/mother_01.png'):
                pil_father = Image.open('gRPC_Server/aligned_images/mother_01.png')
                (fat_width, fat_height) = pil_father.size
                resize_fat = max(fat_width, fat_height)/256
                log("Mother\'s face was uploaded and found\n")
                # display(pil_father.resize((int(fat_width/resize_fat), int(fat_height/resize_fat))))
            else: log('Mother\'s face was not found or there is more than one in the photo.')

        return pkg.UploadImageResponse(answer = answer)

def serve():
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=8))

    pkg_grpc.add_UploadImageServiceServicer_to_server(
        UploadImageService(), server

    )

    server.add_insecure_port("[::]:8082")
    server.start()
    server.wait_for_termination()

if __name__ == "__main__":
    serve()