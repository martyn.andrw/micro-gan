import grpc

import pkg.UploadImages_pb2 as pkg
import pkg.UploadImages_pb2_grpc as pkg_grpc

channel = grpc.insecure_channel("localhost:8082")
client = pkg_grpc.UploadImageServiceStub(channel)

test_request_ryan = pkg.UploadImageRequest(
    meta = pkg.Metadata(parent_type = pkg.Metadata.FATHER), 
    url = "https://upload.wikimedia.org/wikipedia/commons/thumb/f/f6/Ryan_Gosling_in_2018.jpg/800px-Ryan_Gosling_in_2018.jpg"
    )

test_request_hello = pkg.UploadImageRequest(
    meta = pkg.Metadata(parent_type = pkg.Metadata.MOTHER), 
    url = "https://mors-tomsk.sibnet.ru/upload/imgeventsbig/eed58dceaca2ca45d2f3e9d17c94c3b2.jpg"
    )

responce = client.UploadImage(test_request_hello)
print(responce)

responce = client.UploadImage(test_request_ryan)
print(responce)