proto:
	python3 \
		-m grpc_tools.protoc \
		-I gRPC_Server/proto/ \
		--python_out=gRPC_Server/pkg/ \
		--grpc_python_out=gRPC_Server/pkg/ \
		gRPC_Server/proto/UploadImages.proto
	cp -r gRPC_Server/pkg/* gRPC_Client/pkg

server:
	export PYTHONPATH=gRPC_Server/pkg/ && python3 gRPC_Server/main.py

client:
	export PYTHONPATH=gRPC_Client/pkg/ && python3 gRPC_Client/main.py

deps:
	pip3 install h5py
	# !pip3 install -U numpy==1.18.5
	pip3 install numpy
	pip3 install tensorflow
	pip3 install opencv-python
	pip3 install imageio
	pip3 install moviepy
	pip3 install matplotlib
	pip3 install scipy
	pip3 install ipython
	pip3 install sympy
	pip3 install cmake
	pip3 install dlib